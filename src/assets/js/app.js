let documentReady = function (fn) {
    if (document.readyState === 'loading') {
        document.addEventListener('DOMContentLoaded', fn);
    } else {
        fn();
    }
}

documentReady (() => {

    $(".select2").select2({
        // the following code is used to disable x-scrollbar when click in select input and
        // take 100% width in responsive also
        dropdownAutoWidth: true,
        width: '100%'
    });


    document.querySelectorAll('.pretty-scroll').forEach(el => {
        new PerfectScrollbar(el,{
            wheelSpeed: 1,
            wheelPropagation: true,
            minScrollbarLength: 20
        })
    });

    let aside = function () {

        $('.aside-open').on('click', function (e){
            e.preventDefault();
            let asideName = '#' + $(this).attr('data-aside');
            $(asideName).addClass('aside--open');
            $('.aside-overlay').addClass('aside-overlay--open');
        })

        $('.aside-overlay').on('click', function (e){
            e.preventDefault();
            $(this).removeClass('aside-overlay--open');
            $('.aside').removeClass('aside--open');
        })

        $('.btn-aside-close').on('click', function (){
            $('.aside-overlay').removeClass('aside-overlay--open');
            $('.aside').removeClass('aside--open');
        })

    }
    aside();

    $('.pickadate').pickadate({
        formatSubmit: 'dd/mm/yyyy',
        monthsFull: [ 'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь' ],
        monthsShort: [ 'Янв', 'Фев', 'Март', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сен', 'Отк', 'Ноя', 'Дек' ],
        weekdaysShort: [ 'ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ' ],
        today: 'сегодня',
        clear: 'очистить',
        close: 'закрыть',
        autoApply: true,
    });

    $('.pickatime').pickatime({
        format: 'h:i',
        interval: 15,
        minYear: 2010,
        min: new Date(2015,3,20,7),
        max: new Date(2022,7,14,18,30)
    });

    $('.btn-favorite').on('click', function (){
        $(this).toggleClass('btn-favorite--checked');
    })

    $('.btn-icon--event').on('click', function (){
        $(this).toggleClass('checked');
    })

    $('.items-checked-all input').on('change', function (){
        let $this = $(this);
        let checkContainer = $this.closest('.items-check-container');

        if ($this.is(':checked')) {
            checkContainer.find('.pretty-checkbox__input').prop('checked', true);
        }
        else {
            checkContainer.find('.pretty-checkbox__input').prop('checked', false);
        }

    });

    // Grad and drop
    let  containers = $('.drag-container').toArray();
    containers.concat($('.list-item-grad').toArray());
    dragula(containers, {
        isContainer: function (el) {
            return el.classList.contains('drag-container');
        }
    });

    // form repeater jquery
    $('.file-repeater, .contact-repeater, .repeater-default').repeater({
        show: function () {
            $(this).slideDown();
        },
        hide: function (deleteElement) {
            if (confirm('Are you sure you want to delete this element?')) {
                $(this).slideUp(deleteElement);
            }
        }
    });

    jQuery.datetimepicker.setLocale('ru');
    jQuery('.datetimepicker').datetimepicker({
        step:1
    });

    jQuery('.timepicker').datetimepicker({
        datepicker:false,
        format:'H:i',
        step:1
    });

    jQuery('.datepicker').datetimepicker({
        timepicker:false,
        format:'d.m.Y',
    });
});



